/*
 Name:		SB11_initial.cpp
 Created:	10/15/2019 3:07:59 PM
 Author:	gwatb
 Editor:	http://www.visualmicro.com
*/

#include "SB11_initial.h"
#include "SPI.h"
#include "Wire.h"
#include "Adafruit_MCP23017.h"
/********************************************************************************************/
/*Function Declare*/
void board_init(void);
void XpinMode(uint32_t pin, uint32_t mode);
void XpullUp(uint32_t pin, uint8_t pull);
void XdigitalWrite(uint32_t pin, uint32_t value);
int  XdigitalRead(uint32_t pin);
void XportWrite(uint32_t LSB_Pin, uint32_t value);
int  XportRead(uint32_t LSB_Pin);
void XattachInterrupt(uint32_t Xpin, uint32_t Ipin, voidFuncPtr callback, uint32_t mode);
uint8_t XgetLastInterruptPin(uint32_t Ipin);
uint8_t XgetLastInterruptPinValue(uint32_t Xpin);
/********************************************************************************************/
/*Global Parameter*/
Adafruit_MCP23017 Exp_IO[2];
/********************************************************************************************/
void board_init(void) {
	int timeout = 500;

	//Expander IO initial
	Exp_IO[0].begin(0);
	Exp_IO[1].begin(1);
	Wire.setClock(Exp_port_clk);
	
	XpinMode(PIN_Dx0, INPUT);				//Pin A_0 :INPUT MODE
	XpinMode(PIN_Dx1, INPUT);				//Pin A_1 :INPUT MODE
	XpinMode(PIN_Dx2, INPUT);				//Pin A_2 :INPUT MODE
	XpinMode(PIN_Dx3, INPUT);				//Pin A_3 :INPUT MODE
	XpinMode(PIN_Dx4, INPUT);				//Pin A_4 :INPUT MODE
	XpinMode(PIN_Dx5, INPUT);				//Pin A_5 :INPUT MODE
	XpinMode(PIN_Dx6, INPUT);				//Pin A_6 :INPUT MODE
	XpinMode(PIN_Dx7, INPUT);				//Pin A_7 :INPUT MODE
								
	XpinMode(PIN_ESP_RST    , OUTPUT);		//Pin B_0 :OUTPUT MODE
	XpinMode(PIN_ETH_RST    , OUTPUT);		//Pin B_1 :OUTPUT MODE
	XpinMode(PIN_GSM_RST    , OUTPUT);		//Pin B_2 :OUTPUT MODE
	XpinMode(PIN_GSM_DAT_DIS, OUTPUT);		//Pin B_3 :OUTPUT MODE
	XpinMode(PIN_LED0       , OUTPUT);		//Pin B_4 :OUTPUT MODE
	XpinMode(PIN_LED1       , OUTPUT);		//Pin B_5 :OUTPUT MODE
	XpinMode(PIN_LED2       , OUTPUT);		//Pin B_6 :OUTPUT MODE
	XpinMode(PIN_LCD_RST    , OUTPUT);		//Pin B_7 :OUTPUT MODE


	XpullUp(PIN_Dx0        , HIGH);			//Pull up All IO
	XpullUp(PIN_Dx1        , HIGH);
	XpullUp(PIN_Dx2        , HIGH);
	XpullUp(PIN_Dx3        , HIGH);
	XpullUp(PIN_Dx4        , HIGH);
	XpullUp(PIN_Dx5        , HIGH);
	XpullUp(PIN_Dx6        , HIGH);
	XpullUp(PIN_Dx7        , HIGH);
	XpullUp(PIN_ESP_RST    , HIGH);
	XpullUp(PIN_ETH_RST    , HIGH);
	XpullUp(PIN_GSM_RST    , HIGH);
	XpullUp(PIN_GSM_DAT_DIS, HIGH);
	XpullUp(PIN_LED0       , HIGH);
	XpullUp(PIN_LED1       , HIGH);
	XpullUp(PIN_LED2       , HIGH);
	XpullUp(PIN_LCD_RST    , HIGH);

	XdigitalWrite(PIN_ESP_RST	 , LOW);	//WIFI module Reset On Start
	XdigitalWrite(PIN_ETH_RST	 , LOW);	//W5500 Reset On Start
	XdigitalWrite(PIN_GSM_RST	 , LOW);    //GSM module Reset On Start
	XdigitalWrite(PIN_GSM_DAT_DIS, HIGH);	
	XdigitalWrite(PIN_LED0		 , HIGH);
	XdigitalWrite(PIN_LED1		 , HIGH);
	XdigitalWrite(PIN_LED2		 , HIGH);
	XdigitalWrite(PIN_LCD_RST	 , LOW);	//LCD module Reset On Start


	//Internal IO pin Initial
	pinMode(PIN_LORA_SEL	   ,OUTPUT);
	pinMode(PIN_ETH_CS		   ,OUTPUT);
	pinMode(PIN_LORA_RST	   ,OUTPUT);
	pinMode(PIN_GSM_WAKE	   ,INPUT_PULLUP);
	pinMode(PIN_SD_CS		   ,OUTPUT);
	pinMode(PIN_RW485		   ,OUTPUT);
	pinMode(PIN_LORA_DIO0	   ,INPUT_PULLUP);
	pinMode(PIN_USB_HOST_ENABLE,INPUT);
	pinMode(PIN_SWCLK		   ,INPUT);
	pinMode(PIN_SWDIO		   ,INPUT);
	pinMode(PIN_LCD_INT		   ,INPUT_PULLUP);
	pinMode(PIN_EPORT_INT	   ,INPUT_PULLUP);
	pinMode(PIN_ETH_INT		   ,INPUT_PULLUP);

	digitalWrite(PIN_LORA_SEL       , HIGH);
	digitalWrite(PIN_ETH_CS	        , HIGH);
	digitalWrite(PIN_LORA_RST       , LOW);
	digitalWrite(PIN_SD_CS          , HIGH);
	digitalWrite(PIN_RW485          , HIGH);

	SerialUSB.begin(SerialUSB_Baud);
	while ((!SerialUSB)&&(timeout--))delay(1);
	RS485comm.begin(RS485_Baud);
	while (!RS485comm);
	GSMcomm.begin(GSM_Baud);
	while (!GSMcomm);
	WIFIcomm.begin(WIFI_Baud);
	while (!WIFI_Baud);

	XdigitalWrite(PIN_ESP_RST, HIGH);
	XdigitalWrite(PIN_ETH_RST, HIGH);
	XdigitalWrite(PIN_GSM_RST, HIGH);
	digitalWrite(PIN_LORA_RST, HIGH);
}

void XpinMode(uint32_t pin, uint32_t mode) {
	Exp_IO[(pin >> 4) - 3].pinMode(pin & 0xF,mode);
}

void XpullUp(uint32_t pin, uint8_t pull) {
	Exp_IO[(pin >> 4) - 3].pullUp(pin & 0xF,pull);
}

void XdigitalWrite(uint32_t pin, uint32_t value) {
	Exp_IO[(pin >> 4) - 3].digitalWrite(pin & 0xF, value);
}

int XdigitalRead(uint32_t pin) {
	return Exp_IO[(pin >> 4) - 3].digitalRead(pin & 0xF);
}

void XportWrite(uint32_t LSB_Pin,uint32_t value) {
	Exp_IO[(LSB_Pin >> 4) - 3].writeGPIOAB(value<<(LSB_Pin & 0x0F));
}

int XportRead(uint32_t LSB_Pin) {
	return Exp_IO[(LSB_Pin >> 4) - 3].readGPIOAB() >> (LSB_Pin & 0x0F);
}

void XattachInterrupt(uint32_t Xpin,uint32_t Ipin, voidFuncPtr callback, uint32_t mode) {
	uint8_t polarity = 0;
	switch (mode) {
		case FALLING: polarity = LOW ; break;
		case RISING : polarity = HIGH; break;
	}
	Exp_IO[(Xpin >> 4) - 3].setupInterrupts(false, false, polarity);
	XpinMode(Xpin, INPUT);
	XpullUp(Xpin, HIGH);
	Exp_IO[(Xpin >> 4) - 3].setupInterruptPin(Xpin & 0xF, mode);	
	
	pinMode(Ipin, INPUT_PULLUP);
	attachInterrupt(Ipin, callback, mode);
}

uint8_t XgetLastInterruptPin(uint32_t Ipin) {
	uint8_t result = MCP23017_INT_ERR;
	if (Ipin == PIN_EPORT_INT) {
		result = Exp_IO[0].getLastInterruptPin();
		if (result != MCP23017_INT_ERR) return 0x30 + result;
	}
	if (Ipin == PIN_LCD_INT) {
		result = Exp_IO[1].getLastInterruptPin();
		if (result != MCP23017_INT_ERR) return 0x40 + result;
	}
	return MCP23017_INT_ERR;
}

uint8_t XgetLastInterruptPinValue(uint32_t Xpin) {
	if (Xpin != MCP23017_INT_ERR) return Exp_IO[(Xpin >> 4) - 3].digitalRead(Xpin & 0xF);
	else return MCP23017_INT_ERR;
}

