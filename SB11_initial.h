/*
 Name:		SB11_initial.h
 Created:	10/15/2019 3:07:59 PM
 Author:	gwatb
 Editor:	http://www.visualmicro.com
*/

#ifndef _SB11_initial_h
#define _SB11_initial_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"	
#else
	#include "WProgram.h"
#endif



#define Exp_port_clk	800000
#define SerialUSB_Baud	115200
#define RS485_Baud		115200
#define GSM_Baud		115200
#define WIFI_Baud		115200

extern void XpinMode(uint32_t pin, uint32_t mode);
extern void XpullUp(uint32_t pin, uint8_t pull);
extern void XdigitalWrite(uint32_t pin, uint32_t value);
extern int XdigitalRead(uint32_t pin);
extern void XportWrite(uint32_t LSB_Pin, uint32_t value);
extern int XportRead(uint32_t LSB_Pin);
extern void XattachInterrupt(uint32_t Xpin, uint32_t Ipin, voidFuncPtr callback, uint32_t mode);
extern uint8_t XgetLastInterruptPin(uint32_t Ipin);
extern uint8_t XgetLastInterruptPinValue(uint32_t Xpin);
extern void board_init(void);

#endif

